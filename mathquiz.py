from random import randint

num1 = randint(1,10)
num2 = randint(1,10)
question = "What is " + str(num1) + " times " + str(num2) + "? "
for x in range(2):
    answer = int(input(question))
    if num1 * num2 == answer:
        print("That's right - well done. ")
    else:
        print("No, I'm afraid the answer is " + str(num1*num2) + ". ")


