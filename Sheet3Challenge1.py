from gasp import *

begin_graphics()
Circle((310, 210),40)
Circle((295,220),5)
Circle((325,220),5)
Line((300,200),(320,200))
Line((300,200),(310,220))
Arc((310, 210), 30, 225, 315)
update_when('key_pressed')      # you know what this does by now...
end_graphics()
