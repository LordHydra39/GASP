from random import randint

num1 = randint(1,10)
num2 = randint(1,10)
right = 0
for x in range(10):
    num1 = randint(1,10)
    num2 = randint(1,10)
    question = "What is " + str(num1) + " times " + str(num2) + "? "
    answer = int(input(question))
    if num1 * num2 == answer:
        print("That's right - well done. ")
        right = right+1
    else:
        print("No, I'm afraid the answer is " + str(num1*num2) + ". ")
print("I asked you 10 questions.  You got " + str(right) + " of them right.")

print("Well done!")
